.PHONY: test docs

VERSION=$(shell grep "version" ./library.properties | sed 's/version=\([0-9\.]*\)/\1/')

test:
	pio test -e native
	#g++ -DSTANDALONE -Ilib/mcp2515 lib/mcp2515/mcp2515.cpp -o mcp2515

docs:
	LIB_VERSION=$(VERSION) doxygen docs/Doxyfile
