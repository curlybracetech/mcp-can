#ifdef UNIT_TEST

#include <unity.h>
#include "mcpcan.hpp"
#include "sae.hpp"

void test_buildPgnObj(){
  SPN s = uintToSpn(0x0000FF00);
  TEST_ASSERT_EQUAL(s.msb, 0x00);
  TEST_ASSERT_EQUAL(s.b2, 0xFF);
  TEST_ASSERT_EQUAL(s.lsb, 0x00);

  SPN s2 = uintToSpn(0x0000FF00);
}

int main(){
    UNITY_BEGIN();
    RUN_TEST(test_buildPgnObj);
    UNITY_END();
    return 0;
}

#endif
