#ifdef SECRET_FILE

#include <Arduino.h>
#include <SPI.h>
#include <mcpcan.h>

mcp2515* mcp;
int cs_pin = 10;
int stdby_pin = 4;

uint8_t data[8] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};
CANmsg msg0 = CANmsg(0x27, 0, 8, data);
CANmsg msg1 = CANmsg();

void setup(){
    Serial.begin(9600);
    mcp = new mcp2515(cs_pin, stdby_pin);
    Serial.print("Boot");
    mcp->init();
    Serial.print("Running");
}

void loop(){
  //TX test
  //mcp->sendMsgBuf(&msg0, 0);
  //for(int i=0; i < 8; i++){
  //    msg0.setData(i, msg0.getData(i) + 2);
  //}

  //RX test
  if(mcp->dataAvailable()){
      Serial.print("msg: ");
      mcp->readR0(&msg1);
      printMsg(&msg1);
  }
  delay(250);
}

#endif