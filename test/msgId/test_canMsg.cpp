#ifdef UNIT_TEST

#include <unity.h>
#include "mcpcan.hpp"

void test_buildMsgWExtendedId(){
    uint8_t buf[8];

    const uint32_t ADDR1 = 0x18F00401;
    CANmsg msg(ADDR1, 1);
    TEST_ASSERT_EQUAL(ADDR1, msg.getID());

    const uint32_t ADDR2 = 0x123;
    CANmsg msg2(ADDR2, 0);
    TEST_ASSERT_EQUAL(ADDR2, msg2.getID());

    const uint32_t ADDR3 = 0x1FFFFFFF;
    CANmsg msg3(ADDR3, 1);
    TEST_ASSERT_EQUAL(ADDR3, msg3.getID());

    const uint32_t ADDR4 = 0x7FF;
    CANmsg msg4(ADDR4, 0);
    TEST_ASSERT_EQUAL(ADDR4, msg4.getID());

    const uint32_t ADDR5 = 0x1;
    CANmsg msg5(ADDR5, 0);
    TEST_ASSERT_EQUAL(ADDR5, msg5.getID());

    CANmsg msg5b(ADDR5, 1);
    TEST_ASSERT_EQUAL(ADDR5, msg5b.getID());
}

int main(){
    UNITY_BEGIN();
    RUN_TEST(test_buildMsgWExtendedId);
    UNITY_END();
    return 0;
}

#endif
