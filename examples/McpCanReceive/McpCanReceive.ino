#include <mcpcan.hpp>
#include <adcn01.hpp>
#include <SPI.h>

mcp2515 mcp = mcp2515(ADCN01_CS, ADCN01_STDBY);
CANmsg msg0 = CANmsg();

void setup(){
    Serial.begin(9600);
    Serial.println("Boot");
    SPI.begin();
    mcp.hwSetup();
    mcp.init(C16M_B500K);
    Serial.println("Running");
}

void loop(){
  if(mcp.dataAvailable()){
      mcp.readR0(&msg0);
      printCanMsg(&msg0);
  }
  delay(250);
}
