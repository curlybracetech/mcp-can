#include <mcpcan.hpp>
#include <adcn01.hpp>
#include <SPI.h>

mcp2515 mcp = mcp2515(ADCN01_CS, ADCN01_STDBY);

uint8_t data[8] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};
//Creates a new message w/address, (not extended), 8-bytes long, to use 'data'
//as it's data buffer.
CANmsg msg0 = CANmsg(0x27, 0, 8, data);

void setup(){
    Serial.begin(9600);
    Serial.println("Boot");
    SPI.begin();
    mcp.hwSetup();
    mcp.init(C16M_B500K);
    Serial.println("Running");
}

void loop(){
    mcp.sendMsgBuf(&msg0, 0);
    for(int i=0; i < 8; i++){
      msg0.setData(i, msg0.getData(i) + 2);
    }
    delay(10);
}
