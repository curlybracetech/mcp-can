import csv

fd = open('regmap', 'r')
fo = open('regmap.h', 'w')

c = csv.DictReader(fd)
lines = []
otable = {}
for i in range(0, 8):
    otable[i] = {}

for line in c:
    lines.append(line)

for l in lines:
    low_nib = l['H']
    for high_nib, reg_name in l.items():
        if high_nib == 'H':
            continue
        otable[int(high_nib,16)][int(low_nib,16)] = reg_name

for i in range(0,8):
    for j in range(0, 16):
        fo.write("static const u8 REG_%s = 0x%s%s;\n" % (otable[i][j],
            hex(i)[2:].upper(), hex(j)[2:].upper()))

fd.close()
fo.close()

