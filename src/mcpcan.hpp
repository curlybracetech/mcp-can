#ifndef CB_MCPCAN_H_
#define CB_MCPCAN_H_

#ifndef PIO
#define TARGET_ARDUINO
#endif

#include <stdint.h>

#ifdef TARGET_ARDUINO
#include <SPI.h>
#endif

#ifdef TARGET_NATIVE
#include "arduinoMock.h"
#endif

/*
 * REG_ prefix constants are addresses from the listing Table 11-1 of the
 * MCP2515 datasheet. For the multiple locations that have the same name
 * (CANSTAT and CANCTRL), the first possible memory mapping is used.
 */
#define  REG_RXF0SIDH 0x00
#define  REG_RXF0SIDL 0x01
#define  REG_RXF0EID8 0x02
#define  REG_RXF0EID0 0x03
#define  REG_RXF1SIDH 0x04
#define  REG_RXF1SIDL 0x05
#define  REG_RXF1EID8 0x06
#define  REG_RXF1EID0 0x07
#define  REG_RXF2SIDH 0x08
#define  REG_RXF2SIDL 0x09
#define  REG_RXF2EID8 0x0A
#define  REG_RXF2EID0 0x0B
#define  REG_BFPCTRL 0x0C
#define  REG_TXRTSCTRL 0x0D
#define  REG_CANSTAT 0x0E
#define  REG_CANCTRL 0x0F

#define  REG_RXF3SIDH 0x10
#define  REG_RXF3SIDL 0x11
#define  REG_RXF3EID8 0x12
#define  REG_RXF3EID0 0x13
#define  REG_RXF4SIDH 0x14
#define  REG_RXF4SIDL 0x15
#define  REG_RXF4EID8 0x16
#define  REG_RXF4EID0 0x17
#define  REG_RXF5SIDH 0x18
#define  REG_RXF5SIDL 0x19
#define  REG_RXF5EID8 0x1A
#define  REG_RXF5EID0 0x1B
#define  REG_TEC 0x1C
#define  REG_REC 0x1D

#define  REG_RXM0SIDH 0x20
#define  REG_RXM0SIDL 0x21
#define  REG_RXM0EID8 0x22
#define  REG_RXM0EID0 0x23
#define  REG_RXM1SIDH 0x24
#define  REG_RXM1SIDL 0x25
#define  REG_RXM1EID8 0x26
#define  REG_RXM1EID0 0x27
#define  REG_CNF3 0x28
#define  REG_CNF2 0x29
#define  REG_CNF1 0x2A
#define  REG_CANINTE 0x2B
#define  REG_CANINTF 0x2C
#define  REG_EFLG 0x2D

#define  REG_TXB0CTRL 0x30
#define  REG_TXB0SIDH 0x31
#define  REG_TXB0SIDL 0x32
#define  REG_TXB0EID8 0x33
#define  REG_TXB0EID0 0x34
#define  REG_TXB0DLC 0x35
#define  REG_TXB0D0 0x36
#define  REG_TXB0D1 0x37
#define  REG_TXB0D2 0x38
#define  REG_TXB0D3 0x39
#define  REG_TXB0D4 0x3A
#define  REG_TXB0D5 0x3B
#define  REG_TXB0D6 0x3C
#define  REG_TXB0D7 0x3D

#define  REG_TXB1CTRL 0x40
#define  REG_TXB1SIDH 0x41
#define  REG_TXB1SIDL 0x42
#define  REG_TXB1EID8 0x43
#define  REG_TXB1EID0 0x44
#define  REG_TXB1DLC 0x45
#define  REG_TXB1D0 0x46
#define  REG_TXB1D1 0x47
#define  REG_TXB1D2 0x48
#define  REG_TXB1D3 0x49
#define  REG_TXB1D4 0x4A
#define  REG_TXB1D5 0x4B
#define  REG_TXB1D6 0x4C
#define  REG_TXB1D7 0x4D

#define  REG_TXB2CTRL 0x50
#define  REG_TXB2SIDH 0x51
#define  REG_TXB2SIDL 0x52
#define  REG_TXB2EID8 0x53
#define  REG_TXB2EID0 0x54
#define  REG_TXB2DLC 0x55
#define  REG_TXB2D0 0x56
#define  REG_TXB2D1 0x57
#define  REG_TXB2D2 0x58
#define  REG_TXB2D3 0x59
#define  REG_TXB2D4 0x5A
#define  REG_TXB2D5 0x5B
#define  REG_TXB2D6 0x5C
#define  REG_TXB2D7 0x5D

#define  REG_RXB0CTRL 0x60
#define  REG_RXB0SIDH 0x61
#define  REG_RXB0SIDL 0x62
#define  REG_RXB0EID8 0x63
#define  REG_RXB0EID0 0x64
#define  REG_RXB0DLC 0x65
#define  REG_RXB0D0 0x66
#define  REG_RXB0D1 0x67
#define  REG_RXB0D2 0x68
#define  REG_RXB0D3 0x69
#define  REG_RXB0D4 0x6A
#define  REG_RXB0D5 0x6B
#define  REG_RXB0D6 0x6C
#define  REG_RXB0D7 0x6D

#define  REG_RXB1CTRL 0x70
#define  REG_RXB1SIDH 0x71
#define  REG_RXB1SIDL 0x72
#define  REG_RXB1EID8 0x73
#define  REG_RXB1EID0 0x74
#define  REG_RXB1DLC 0x75
#define  REG_RXB1D0 0x76
#define  REG_RXB1D1 0x77
#define  REG_RXB1D2 0x78
#define  REG_RXB1D3 0x79
#define  REG_RXB1D4 0x7A
#define  REG_RXB1D5 0x7B
#define  REG_RXB1D6 0x7C
#define  REG_RXB1D7 0x7D

/* Flag masks for the CANINTE interrupt enable register - used to enable/disable
 * particular interrupt conditions
 */
#define MASK_CANINTE_RX0IF 0x01
#define MASK_CANINTE_RX1IF 0x02
#define MASK_CANINTE_TX0IF 0x04
#define MASK_CANINTE_TX1IF 0x08
#define MASK_CANINTE_TX2IF 0x10
#define MASK_CANINTE_ERRIF 0x20
#define MASK_CANINTE_WAKIF 0x40
#define MASK_CANINTE_MERRF 0x80

/* Flag masks for the CANINTF control register - used to indicate the presence
 * of interrupt conditions
 */
#define MASK_CANINTF_RX0IF 0x01
#define MASK_CANINTF_RX1IF 0x02
#define MASK_CANINTF_TX0IF 0x04
#define MASK_CANINTF_TX1IF 0x08
#define MASK_CANINTF_TX2IF 0x10
#define MASK_CANINTF_ERRIF 0x20
#define MASK_CANINTF_WAKIF 0x40
#define MASK_CANINTF_MERRF 0x80

/* Flag masks for the CANCTRL register individual bit control
 */
#define MASK_CANCTRL_REQOP  0xE0
#define MASK_CANCTRL_ABAT   0x10
#define MASK_CANCTRL_OSM    0x08
#define MASK_CANCTRL_CLKEN  0x04
#define MASK_CANCTRL_CLKPRE 0x03

/* Flag masks for the RXB0CTRL register individual variables
 */
#define MASK_RXB0CTRL_RXM    0x60
#define MASK_RXB0CTRL_RXRTR  0x08
#define MASK_RXB0CTRL_BUKT   0x04
#define MASK_RXB0CTRL_BUKT1  0x02
#define MASK_RXB0CTRL_FILHIT 0x01

/* Flag masks for the results of the 'READ STATUS' command, a quick-reference
 * commmand that assembles results from 4 seperate registers. See MCP2515
 * manual p67
 */
#define RDSTAT_RX0IF  0x01
#define RDSTAT_RX1IF  0x02
#define RDSTAT_TX0REQ 0x04
#define RDSTAT_TX0IF  0x08
#define RDSTAT_TX1REQ 0x10
#define RDSTAT_TX1IF  0x20
#define RDSTAT_TX2REQ 0x40
#define RDSTAT_TX2IF  0x80

/* CMD_ prefix constants are the command words specified as part of the SPI
 * communications protocol between the master device and the MCP2515. Most are
 * defined in Table 12-1 of the MCP2515 datasheet.
 */
#define CMD_RESET 0xC0
#define CMD_READ  0x03
#define CMD_WRITE 0x02
#define CMD_STATUS 0xA0
#define CMD_RXSTAT 0xB0
#define CMD_BITMOD 0x05

/* Read RX Buffer commands. Special commands to bypass overhead of explicitly
 * setting address to read from in case of reception buffer. Begins reading from
 * either RXBnSIDH (ID version) or RXBnD0 (DATA version)
 */
#define CMD_READRX_B0ADDR 0x90
#define CMD_READRX_B0DATA 0x92
#define CMD_READRX_B1ADDR 0x94
#define CMD_READRX_B1DATA 0x96

/* Write TX Buffer commands. Special commands to bypass overhead of explicitly
 * setting address to write to in case of transmission buffer. Begins writing to
 * either TXBnSIDH (ID version) or TXBnD0 (DATA version)
 */
#define CMD_LOADTX_TXB0SIDH 0x40
#define CMD_LOADTX_TXB0D0   0x41
#define CMD_LOADTX_TXB1SIDH 0x42
#define CMD_LOADTX_TXB1D0   0x43
#define CMD_LOADTX_TXB2SIDH 0x44
#define CMD_LOADTX_TXB2D0   0x45

#define CMD_RTS_BX 0x80
#define CMD_RTS_B0 0x81
#define CMD_RTS_B1 0x82
#define CMD_RTS_B2 0x84

/* MODE_ prefix constants describe the operational modes available to the
 * controller as per chapter 10.0 of the MCP2515 manual
 */
#define MODE_CONFIG    0x80
#define MODE_NORMAL    0x00
#define MODE_SLEEP     0x20
#define MODE_LISTEN    0x60
#define MODE_LOOPBACK  0x40

/* OSM_ prefix constants are values pertinent to the One Shot Mode bits at
 * CANCTRL.OSM (see register 10-1 in the manual)
 */
#define OSM_ENABLE     0x08
#define OSM_DISABLE    0x00

//----------------------  CLOCK SETTINGS  ------------------------------------
typedef struct CNF {
    uint8_t cnf1;
    uint8_t cnf2;
    uint8_t cnf3;
} CnfSet;

//TODO - Convert to runtime determination

// 16Mhz Clock for 1MB Baud Rate
extern CnfSet C16M_B1M;
// 16Mhz Clock for 800Kb Baud Rate
//extern CnfSet C16M_B800K;
extern CnfSet C16M_B500K;
extern CnfSet C16M_B250K;
extern CnfSet C16M_B125K;
extern CnfSet C16M_B100K;
extern CnfSet C16M_B50K;
extern CnfSet C16M_B20K;
extern CnfSet C16M_B10K;

//------------------  END CLOCK SETTINGS  ------------------------------------

//Maxium ID value for extended ID family (29-bit)
#define ID_EXT_MAX 0x1FFFFFFF
//Maxium ID value for standard ID family (11-bit)
#define ID_STD_MAX 0x7FF

#define DIR_WRITE 1
#define DIR_READ 0
#define NOPIN 0xFF

/*
 * Modifies a portion of reg based on val/valmask/shift, leaves other portions
 * of reg in place.
 *
 * reg - pointer to the destination register
 * mask - the mask which which selects the bits of the value post shift
 * shift - the number of positions to left shift the val before applying it to
 *         the desination register
 * val - the value to apply, contained in the lowest order bits
 */
void regApplyBits8(uint8_t* reg, uint8_t valmask, uint8_t shift, uint8_t val);

/*
 * reg - pointer to the source register
 * valmask - the mask which when AND'ed with the register will retain only the
 *           region to be retrieved
 *  shift - the number of positions to right shift the regsiter contents before
 *          returning them
 */
uint8_t regGetBits8(uint8_t* reg, uint8_t valmask, uint8_t shift);

class CANmsg {
    friend class mcp2515;
    private:
        uint8_t msg[13];

        inline uint8_t* data(){ return &msg[5]; }
        inline uint8_t* ctrl(){ return &msg[0]; }

        inline uint8_t* SIDH(){ return &msg[0]; }
        inline uint8_t* SIDL(){ return &msg[1]; }
        inline uint8_t* EID8(){ return &msg[2]; }
        inline uint8_t* EID0(){ return &msg[3]; }
        inline uint8_t* DLC() { return &msg[4]; }

        void setExtendedID(uint8_t ide);
        uint8_t getExtendedID();
        void setLength(uint8_t len);

        void setCtrlBuf(uint8_t len, uint8_t* buf);
        uint8_t* getCtrlBuf();

    public:
        CANmsg();

        /**
         *  @param id CAN ID to apply to the message
         *  @param extended True/false indicating if address is an extended (true) or
         *                  standard length (false) ID
         *  @param len Length of the packet in bytes
         *  @param buf Pointer to a buffer which will provide initial values for the
         *             packet data
         */
        CANmsg(uint32_t id, uint8_t extended, uint8_t len, uint8_t* buf);
        CANmsg(uint32_t id, uint8_t extended);

        uint8_t getLength();

        /**
         * @brief Sets the "remote transmit request" bit in the DLC (data
         *        length code register. RTR packets are used to request data
         *        from other devices on the network.
         *
         *  @param rtr Value of the RTR bit to set (valid values: 0 or 1)
         */
        void setRemoteTransmit(uint8_t rtr);
        uint8_t getRemoteTransmit();

        void setDataBuf(uint8_t len, uint8_t* buf);
        uint8_t* getDataBuf();

        void setData(uint8_t position, uint8_t d);
        uint8_t getData(uint8_t position);
        uint8_t getCtrl(uint8_t position);

        uint32_t getID();
        uint32_t setID(uint32_t id, uint8_t extended);

        void dbgMsg();
};

void printCanMsg(CANmsg *msg);

class mcp2515 {
    private:
        uint8_t CSpin;        //!< Pin used for chip-select for this peripheral
        uint8_t TRXSTNDBYpin; //!< Pin used to suppress IO at MCP2562 transciever
        uint8_t INTpin;       //!< Pin used for interrupt for this peripheral
        uint8_t DRpin;        //!< Pin used for 'data-ready' for this peripheral

        uint8_t RX0BFpin; //RX0BF pin
        uint8_t RX1BFpin; //RX1BF pin
        uint8_t TX0RTSpin;
        uint8_t TX1RTSpin;
        uint8_t TX2RTSpin;

        uint8_t INTflags;      //!< Maintains a local copy of CANINTE (interrupt enable flags)

        SPISettings spiconfig;

        void setupClock(CnfSet& cfg);

        /**
         * @brief Set the operational mode of the controller
         *
         * @param The mode to set the device into, must be chosen from the
         *        MODE_ constants. See chapter 10 in MCP2515 manual.
         */
        void setMode(uint8_t mode);

    public:
        mcp2515(uint8_t cspin);
        mcp2515(uint8_t cspin, uint8_t stdbypin);
        mcp2515(uint8_t cspin, uint8_t stdbypin, uint8_t intpin);
        mcp2515(uint8_t cspin, uint8_t stdbypin, uint8_t intpin, uint8_t drpin);
        mcp2515(uint8_t cspin, uint8_t stdbypin, uint8_t intpin, uint8_t drpin,
                uint8_t t0rtspin, uint8_t t1rtspin, uint8_t t2rtspin);

        void generalComm(uint8_t dir, uint8_t cmdWord, uint8_t startAddr,
                          uint8_t exchange[], uint8_t len);
        void generalRead(uint8_t cmdWord, uint8_t startAddr,
                         uint8_t response[], uint8_t len);

        /**
         * @brief Write a block of data to the device.
         *
         *
         * @param cmdWord The command word (instruction) sent prior to the
         *                data, this may be CMD_WRITE, CMD_LOADTX_TXB0SIDH,
         *                CMD_LOADTX_TXB0D0, or similar.
         * @param startAddr The register address at which to start the write.
         *                  Use 0x0 if the start address is implicit to the
         *                  command.
         * @param data Buffer of data to be written.
         * @param len Length of data to be written from buffer (do not exceede
         *            sizeof(data).
         */
        void generalWrite(uint8_t cmdWord, uint8_t startAddr, uint8_t data[],
                                   uint8_t len);

        /**
         * @brief Modify individual bits within certain registers
         *
         * @param regAddr Address of the register to modify.
         * @param mask Mask indicating which bits in the designated register
         *        will be overwritten. Positions with a 0b1 will be
         *        overwritten, positions with a 0b0 value will be preserved.
         * @param data The (masked bits will be ignored).
         */
        void bitModify(uint8_t regAddr, uint8_t mask, uint8_t data);
        void readRegisterBuf(uint8_t startAddr, uint8_t response[], uint8_t len);

        /**
         * @brief Read a single register.
         *
         * @param regAddr The address of the register to read
         *
         * @return The contents of the register are returned.
         */
        uint8_t readRegister(uint8_t regAddr);

        /**
         * @brief Sends the "Read Status" command, which returns a byte containing
         *        common status information.
         *
         * @return A single byte with the following bit mapping:
         *         bit 0 - CANINTF.RX0IF
         *         bit 1 - CANINTF.RX1IF
         *         bit 2 - TXB0CNTRL.TXREQ
         *         bit 3 - CANINTF.TX0IF
         *         bit 4 - TXB1CNTRL.TXREQ
         *         bit 5 - CANINTF.TX1IF
         *         bit 6 - TXB2CNTRL.TXREQ
         *         bit 7 - CANINTF.TX2IF
         */
        uint8_t readStatus();
        uint8_t readRXStatus();
        uint8_t readCanStatus();
        uint8_t readRxErrorCount();
        uint8_t readTxErrorCount();

        /**
         * @brief Write a value to a single register
         */
        void writeRegister(uint8_t regAddr, uint8_t val);

        void readR0(CANmsg *msg);
        void readR0data(CANmsg *msg);
        void readR1(CANmsg *msg);
        void readR1data(CANmsg *msg);
        void reset();

        int8_t RTS(uint8_t buffer);
        int8_t RTS0();
        int8_t RTS1();
        int8_t RTS2();

        /**
         * @brief Load a transmit buffer on the device with control and packet
         *        data from a CANmsg object.
         *
         * @param msg The CANmsg object holding the control/packet data to be
         *            written.
         * @param cmd The command word to be used for the load action. Should
         *            be from the CMD_LOADTX_TXBNSIDH series of commands.
         */
        void loadT(CANmsg *msg, uint8_t cmd);

        /**
         * @brief Load a transmit buffer on the device with packet data from a
         *        CANmsg object.
         *
         * @param msg The CANmsg object holding the packet data to be written.
         * @param cmd The command word to be used for the load action. Should
         *            be from the CMD_LOADTX_TXBND0 series of commands.
         */
        void loadTdata(CANmsg *msg, uint8_t cmd);
        void loadT0(CANmsg *msg);
        void loadT0data(CANmsg *msg);
        void loadT1(CANmsg *msg);
        void loadT1data(CANmsg *msg);
        void loadT2(CANmsg *msg);
        void loadT2data(CANmsg *msg);

        /**
         * @brief Prepare the Arduino for communication with the MCP2515:
         * Activate SPI and prepare all required DIO for usage.
         */
        void hwSetup();

        /**
         * @brief Prepare the MCP2515 for operation: soft reset the chip and setup
         * the clock.
         */
        void init(CnfSet& cfg, bool oneShot=true);

        /**
         * @brief Load and trigger the transmission of a control and data
         *        packet as a single operation.
         *
         * @param msg The CANmsg holding the packet data to write
         * @param TXbuffNum The transmit buffer to use for the transmission
         *                  (0-2)
         *
         * @return Returns 0 on success, -1 on failure
         */
        int8_t sendMsgBuf(CANmsg *msg, uint8_t TXbuffNum);

        /**
         * @brief Load and trigger the transmission of a data packet as a
         *        single operation.
         *
         * @param msg The CANmsg holding the packet data to write
         * @param TXbuffNum The transmit buffer to use for the transmission (0-2)
         *
         * @return Returns 0 on success, -1 on failure
         */
        int8_t sendMsgBufData(CANmsg *msg, uint8_t TXbuffNum);
        //void sendMsg(CANmsg *msg);

        /**
         * @brief Check if data is available in the recieve buffers
         *
         * @return Returns bitflags - Bit0(LSB) Set --> Data availible in
         *         buffer0;
         *         Bit1 Set --> Data available in buffer1
         */
        uint8_t dataAvailable();
        int8_t readMessage(uint8_t channel, CANmsg *msg);

};

#endif
