#ifndef CB_ADCN01_H_
#define CB_ADCN01_H_

#define ADCN01_CS    10
#define ADCN01_STDBY 4
#define ADCN01_INT   2

#define ADCN01_BF0   3
#define ADCN01_BF1   8
#define ADCN01_RTS0  5
#define ADCN01_RTS1  6
#define ADCN01_RTS2  7

#endif
