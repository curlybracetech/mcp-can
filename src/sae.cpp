#include "sae.hpp"

J1939msg::J1939msg(){}

J1939msg::J1939msg(uint32_t id, uint8_t len, uint8_t* buf): CANmsg(id, 1, len, buf){}

J1939msg::J1939msg(uint8_t pri, uint8_t source, uint32_t pgn, uint8_t len,
    uint8_t* buf): CANmsg(build1939address(pri, source, pgn), 1, len, buf){}

uint32_t build1939address(uint8_t pri, uint8_t source, uint32_t pgn){
    uint32_t addr = 0;
    addr = pri & 0x07;
    addr <<= 26;                     //3-bit priority in bits 26-28
    addr |= (pgn & 0x03FFFF) << 8;   //18-bit pgn in bits 8-25
    addr |= (source);                //8-bit source in bits 0-7
    return addr;
}

uint8_t J1939msg::getPriority(){
    uint32_t id = getID();
    uint32_t pri = (id & 0x1C000000) >> 26;
    return (uint8_t)pri;
}

uint32_t J1939msg::getPGN(){
    uint32_t id = getID();
    uint32_t pgn = (id & 0x03FFFF00) >> 8;
    return pgn;
}

uint8_t J1939msg::getSource(){
    uint32_t id = getID();
    uint32_t src = (id & 0x000000FF);
    return (uint8_t)src;
}

uint8_t J1939msg::getDataPage(){
    uint32_t pgn = getPGN();
    uint32_t dp = (pgn & 0x00010000) >> 16;
    return (uint8_t)dp;
}

uint8_t J1939msg::getPF(){
    uint32_t pgn = getPGN();
    uint32_t pf = (pgn & 0x0000FF00) >> 8;
    return (uint8_t)pf;
}

uint8_t J1939msg::getPS(){
    uint32_t pgn = getPGN();
    uint32_t ps = (pgn & 0x000000FF);
    return (uint8_t)ps;
}

uint32_t spnToUint(SPN const spn){
  uint32_t u = spn.msb | 0x07;
  u <<= 16;
  u |= (spn.b2 << 8);
  u |= spn.lsb;
  return u;
}

SPN uintToSpn(uint32_t const u){
  SPN s;
  s.msb = (u & 0x00070000) >> 16;
  s.b2 = (u & 0x0000FF00) >> 8;
  s.lsb = (u & 0x000000FF);
  return s;
}


