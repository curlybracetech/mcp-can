#ifndef CB_J1939_H_
#define CB_J1939_H_

#include <stdint.h>
#include "mcpcan.hpp"

#ifdef TARGET_ARDUINO
#include <Arduino.h>
#endif

#ifdef TARGET_NATIVE
#include <string.h>
#endif

class J1939msg : public CANmsg {
    public:
        uint8_t getPriority();
        uint32_t getPGN();
        uint8_t getSource();
        uint8_t getDataPage();
        uint8_t getPF();
        uint8_t getPS();

        uint16_t getTransportNumBytes();
        uint8_t getTransportNumPackages();
        uint32_t getTransportPGN();
        uint8_t getTransportSequenceNum();

        J1939msg();
        J1939msg(uint32_t id, uint8_t len, uint8_t* buf);
        J1939msg(uint8_t pri, uint8_t source, uint32_t pgn, uint8_t len, uint8_t* buf);
};

uint32_t build1939address(uint8_t pri, uint8_t source, uint32_t pgn);

typedef struct{
  uint8_t msb;
  uint8_t b2;
  uint8_t lsb;
} j1939triple;

//18bits (extended to 24)
typedef j1939triple PGN;

//19 bits
typedef j1939triple SPN;

uint32_t spnToUint(SPN const spn);
SPN uintToSpn(uint32_t const u);

template<size_t n>
class TransportGroup {
    private:
        uint8_t buf[n];
        size_t bufIdx; //Next available index in the storage buffer
        uint8_t nextMsg; //Expected sequence number of next message

    public:
        J1939msg connectionMsg;
        TransportGroup() : bufIdx(0), nextMsg(1) {}

        void clear(){
            for(size_t i = 0; i < n; i++){
                buf[i] = 0;
            }
            bufIdx = 0;
            nextMsg = 1;
        }

        void* startSequence(J1939msg &msg){
            return memcpy(&connectionMsg, &msg, sizeof(J1939msg));
        }

        void addMsgData(J1939msg &msg){
            if(getSequenceNum(msg) == nextMsg){
                uint16_t bytesAvail = (dataRemaining() < 7) ? dataRemaining() : 7;
                for(uint16_t i = 0; i < bytesAvail; i++){
                    buf[bufIdx++] = msg.getData(i+1);
                }
                nextMsg++;
            }else{
                //TODO: Sequence out of order or data improperly sent to be copied
            }
        }

        uint8_t getData(size_t position){
            if(position < bufIdx){
                return buf[position];
            }
            return 0;
        }

        uint8_t getSource(){
            return connectionMsg.getSource();
        }

        size_t dataRemaining(){
            return getTransportNumBytes() - bufIdx;
        }

        //Get the total number of bytes expected in the sequence
        uint16_t getTransportNumBytes(){
            uint16_t size = 0;
            size |= connectionMsg.getData(2);
            size <<= 8;
            size |= connectionMsg.getData(1);
            return size;
        }

        //Get the total number of packets expected in the sequence
        uint8_t getTransportNumPackages(){
            return connectionMsg.getData(3);
        }

        //Get the PGN for the entire transport sequence
        uint32_t getPGN(){
            uint32_t id = 0;
            id |= connectionMsg.getData(7);
            id <<= 8;
            id |= connectionMsg.getData(6);
            id <<= 8;
            id |= connectionMsg.getData(5);
            return id;
        }

        uint8_t getSequenceNum(J1939msg &msg){
            return msg.getData(0);
        }

        void dbgSeq(HardwareSerial& ser){
            for(size_t i = 0; i < getTransportNumBytes(); i++){
                ser.print(buf[i], HEX);
                ser.print(" ");
            }
        }
};
#endif
