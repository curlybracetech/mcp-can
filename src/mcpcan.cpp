#ifndef PIO
#define TARGET_ARDUINO
#endif

#ifdef TARGET_ARDUINO
#include <Arduino.h>
#include <SPI.h>
#endif

#include "mcpcan.hpp"
#ifdef TARGET_NATIVE
//  SPISettings;
  static spi SPI;
  static HardwareSerial Serial;
#endif

void regApplyBits8(uint8_t* reg, uint8_t mask, uint8_t shift, uint8_t val){
    *reg = (*reg & ~mask) | ((val << shift) & mask);
}

uint8_t regGetBits8(uint8_t* reg, uint8_t valmask, uint8_t shift){

    return ((*reg & valmask) >> shift);
}

CANmsg::CANmsg(){
    for(uint8_t i = 0; i < sizeof(msg); i++){
        msg[i] = 0;
    }
}

CANmsg::CANmsg(uint32_t id, uint8_t extended, uint8_t len, uint8_t* buf){
    for(uint8_t i = 0; i < sizeof(msg); i++){
        msg[i] = 0;
    }

    setID(id, extended);
    setDataBuf(len, buf);
}

//Remote Transmit request version
CANmsg::CANmsg(uint32_t id, uint8_t extended){
    for(uint8_t i = 0; i < sizeof(msg); i++){
        msg[i] = 0;
    }

    setID(id, extended);
    setRemoteTransmit(1);
}

void CANmsg::setLength(uint8_t len){
    regApplyBits8(DLC(), 0x0F, 0, len);
}

uint8_t CANmsg::getLength(){
    uint8_t len = regGetBits8(DLC(), 0x0F, 0);
    if(len > 8){
        len = 8;
    }
    return len;
}

void CANmsg::setRemoteTransmit(uint8_t rtr){
    regApplyBits8(DLC(), 0x40, 6, rtr);
}

uint8_t CANmsg::getRemoteTransmit(){
    return regGetBits8(DLC(), 0x40, 6);
}

void CANmsg::setExtendedID(uint8_t ide){
    regApplyBits8(SIDL(), 0x08, 3, ide);
}

uint8_t CANmsg::getExtendedID(){
    return regGetBits8(SIDL(), 0x08, 3);
}

uint32_t CANmsg::getID(){
    uint32_t addr = ((uint32_t)*SIDH() << 3) | ((uint32_t)*SIDL() >> 5);
    if(getExtendedID()){
        addr <<= 18;
        addr |= (((uint32_t)*SIDL() & 0x03) << 16) | ((uint32_t)*EID8() << 8) |
                (uint32_t)*EID0();
    }
    return addr;
}

uint32_t CANmsg::setID(uint32_t id, uint8_t extended){
    if(extended){
        if(id > ID_EXT_MAX){
            return -1;
        }
        //Indicate that this identifier is extended
        setExtendedID(1);

        *EID0() = (uint8_t)(id & 0xFF);
        *EID8() = (uint8_t)((id >> 8) & 0xFF);
        regApplyBits8(SIDL(), 0x03, 0, ((id >> 16) & 0x03));
        regApplyBits8(SIDL(), 0xE0, 5, ((id >> 18) & 0x07));
        regApplyBits8(SIDH(), 0x07, 0, ((id >> 21) & 0x07));
        regApplyBits8(SIDH(), 0xF8, 3, ((id >> 24) & 0x1F));
    }else{
        if(id > ID_STD_MAX){
            return -1;
        }
        *SIDH() = (uint8_t)(id >> 3);
        regApplyBits8(SIDL(), 0xE0, 5, id);
    }

    return 0;
}

void CANmsg::setDataBuf(uint8_t len, uint8_t* buf){
    uint8_t* d = data();
    for(int i=0; i < len; i++){
        *(d+i) = buf[i];
    }
    setLength(len);
}

uint8_t* CANmsg::getDataBuf(){
    return data();
}

void CANmsg::setData(uint8_t position, uint8_t d){
    *(data()+position) = d;
}

uint8_t CANmsg::getData(uint8_t position){
    return *(data()+position);
}

uint8_t CANmsg::getCtrl(uint8_t position){
    return *(ctrl()+position);
}

void CANmsg::setCtrlBuf(uint8_t len, uint8_t* buf){
    uint8_t *c = msg;
    for(int i=0; i < len; i++){
        *(c+i) = buf[i];
    }
}

void CANmsg::dbgMsg(){
    Serial.print("SIDH: ");
    Serial.print(*SIDH(), HEX);
    Serial.print("\n");

    Serial.print("SIDL: ");
    Serial.print(*SIDL(), HEX);
    Serial.print("\n");

    Serial.print("EID8: ");
    Serial.print(*EID8(), HEX);
    Serial.print("\n");

    Serial.print("EID0: ");
    Serial.print(*EID0(), HEX);
    Serial.print("\n");

    Serial.print("DLC: ");
    Serial.print(*DLC(), HEX);
    Serial.print("\n");

    Serial.print("DATA: ");
    for(uint8_t i = 0; i < 8; i++){
        Serial.print(getData(i), HEX);
        Serial.print(" ");
    }
    Serial.print("\n");
}

mcp2515::mcp2515(uint8_t cspin):
    CSpin(cspin), TRXSTNDBYpin(NOPIN), INTpin(NOPIN), DRpin(NOPIN),
    TX0RTSpin(NOPIN), TX1RTSpin(NOPIN), TX2RTSpin(NOPIN),
    spiconfig(10000000, MSBFIRST, SPI_MODE0)
{}

mcp2515::mcp2515(uint8_t cspin, uint8_t stdbypin):
    CSpin(cspin), TRXSTNDBYpin(stdbypin), INTpin(NOPIN), DRpin(NOPIN),
    TX0RTSpin(NOPIN), TX1RTSpin(NOPIN), TX2RTSpin(NOPIN),
    spiconfig(10000000, MSBFIRST, SPI_MODE0)
{}

mcp2515::mcp2515(uint8_t cspin, uint8_t stdbypin, uint8_t intpin):
    CSpin(cspin), TRXSTNDBYpin(stdbypin), INTpin(intpin), DRpin(NOPIN),
    TX0RTSpin(NOPIN), TX1RTSpin(NOPIN), TX2RTSpin(NOPIN),
    spiconfig(10000000, MSBFIRST, SPI_MODE0)
{}

mcp2515::mcp2515(uint8_t cspin, uint8_t stdbypin, uint8_t intpin,
                 uint8_t drpin):
    CSpin(cspin), TRXSTNDBYpin(stdbypin), INTpin(intpin), DRpin(drpin),
    TX0RTSpin(NOPIN), TX1RTSpin(NOPIN), TX2RTSpin(NOPIN),
    spiconfig(10000000, MSBFIRST, SPI_MODE0)
{}

mcp2515::mcp2515(uint8_t cspin, uint8_t stdbypin, uint8_t intpin,
        uint8_t drpin, uint8_t t0rtspin, uint8_t t1rtspin, uint8_t t2rtspin):
    CSpin(cspin), TRXSTNDBYpin(stdbypin), INTpin(intpin), DRpin(drpin),
    TX0RTSpin(t0rtspin), TX1RTSpin(t1rtspin), TX2RTSpin(t2rtspin),
    spiconfig(10000000, MSBFIRST, SPI_MODE0)
{}

void mcp2515::setupClock(CnfSet& cfg){
    writeRegister(REG_CNF1, cfg.cnf1);
    writeRegister(REG_CNF2, cfg.cnf2);
    writeRegister(REG_CNF3, cfg.cnf3);
}

void mcp2515::setMode(uint8_t mode){
    bitModify(REG_CANCTRL, MASK_CANCTRL_REQOP, mode);
}

void mcp2515::generalComm(uint8_t dir, uint8_t cmdWord, uint8_t startAddr,
                          uint8_t exchange[], uint8_t len){
    SPI.beginTransaction(spiconfig);
    digitalWrite(CSpin, LOW);
    SPI.transfer(cmdWord);
    if(startAddr){
        SPI.transfer(startAddr);
    }
    for(uint8_t i = 0; i < len; i++){
        if(dir == DIR_WRITE){
            SPI.transfer(exchange[i]);
        }else{
            exchange[i] = SPI.transfer(0x00);
        }
    }
    digitalWrite(CSpin, HIGH);
    SPI.endTransaction();
}

void mcp2515::generalRead(uint8_t cmdWord, uint8_t startAddr,
        uint8_t response[], uint8_t len){
    generalComm(DIR_READ, cmdWord, startAddr, response, len);
}

void mcp2515::generalWrite(uint8_t cmdWord, uint8_t startAddr, uint8_t data[],
        uint8_t len){
    generalComm(DIR_WRITE, cmdWord, startAddr, data, len);
}

void mcp2515::bitModify(uint8_t regAddr, uint8_t mask, uint8_t data){
    uint8_t buf[2] = {mask, data};
    generalWrite(CMD_BITMOD, regAddr, buf, 2);
}

void mcp2515::readRegisterBuf(uint8_t startAddr, uint8_t response[], uint8_t len){
    generalRead(CMD_READ, startAddr, response, len);
}

uint8_t mcp2515::readRegister(uint8_t regAddr){
    uint8_t r;
    generalRead(CMD_READ, regAddr, &r, 1);
    return r;
}

uint8_t mcp2515::readStatus(){
    uint8_t res = 0;
    generalRead(CMD_STATUS, 0, &res, 1);
    return res;
}

uint8_t mcp2515::readRXStatus(){
    uint8_t res = 0;
    generalRead(CMD_RXSTAT, 0, &res, 1);
    return res;
}

uint8_t mcp2515::readCanStatus(){
    return readRegister(REG_CANSTAT);
}

uint8_t mcp2515::readRxErrorCount(){
    return readRegister(REG_REC);
}

uint8_t mcp2515::readTxErrorCount(){
    return readRegister(REG_TEC);
}

void mcp2515::writeRegister(uint8_t regAddr, uint8_t val){
    generalWrite(CMD_WRITE, regAddr, &val, 1);
}

void mcp2515::readR0(CANmsg *msg){
    generalRead(CMD_READRX_B0ADDR, 0, msg->ctrl(), 13);
}

void mcp2515::readR0data(CANmsg *msg){
    generalRead(CMD_READRX_B0DATA, 0, msg->ctrl(), 8);
}

void mcp2515::readR1(CANmsg *msg){
    generalRead(CMD_READRX_B1ADDR, 0, msg->ctrl(), 13);
}

void mcp2515::readR1data(CANmsg *msg){
    generalRead(CMD_READRX_B1DATA, 0, msg->ctrl(), 8);
}

void mcp2515::reset(){
    generalWrite(CMD_RESET, 0, NULL, 0);
}

int8_t mcp2515::RTS(uint8_t buffer){
    uint8_t cmd;
    uint8_t pin;
    switch(buffer){
        case 0:
            cmd = CMD_RTS_B0;
            pin = TX0RTSpin;
            break;
        case 1:
            cmd = CMD_RTS_B1;
            pin = TX1RTSpin;
            break;
        case 2:
            cmd = CMD_RTS_B2;
            pin = TX2RTSpin;
            break;
        default:
            return -1;
    }

    //If we have RTS pins available, use them. Otherwise trigger the message
    //send via SPI
    if(pin != NOPIN){
        pinMode(pin, HIGH);
        //wait(howlong?);
        pinMode(pin, LOW);
    }else{
        generalWrite(cmd, 0, NULL, 0);
    }

    return 0;
}

int8_t mcp2515::RTS0(){ return RTS(0); }
int8_t mcp2515::RTS1(){ return RTS(1); }
int8_t mcp2515::RTS2(){ return RTS(2); }

void mcp2515::loadT(CANmsg *msg, uint8_t cmd){
    uint8_t len = msg->getLength();
    generalWrite(cmd, 0, msg->ctrl(), 5+len);
}

void mcp2515::loadTdata(CANmsg *msg, uint8_t cmd){
    uint8_t len = msg->getLength();
    generalWrite(cmd, 0, msg->getDataBuf(), len);
}

void mcp2515::loadT0(CANmsg *msg){
    loadT(msg, CMD_LOADTX_TXB0SIDH);
}

void mcp2515::loadT0data(CANmsg *msg){
    loadTdata(msg, CMD_LOADTX_TXB0D0);
}

void mcp2515::loadT1(CANmsg *msg){
    loadT(msg, CMD_LOADTX_TXB1SIDH);
}

void mcp2515::loadT1data(CANmsg *msg){
    generalWrite(CMD_LOADTX_TXB1D0, 0, msg->getDataBuf(), 8);
}

void mcp2515::loadT2(CANmsg *msg){
    loadT(msg, CMD_LOADTX_TXB2SIDH);
}

void mcp2515::loadT2data(CANmsg *msg){
    generalWrite(CMD_LOADTX_TXB2D0, 0, msg->getDataBuf(), 8);
}

void mcp2515::hwSetup(){
    if(DRpin != NOPIN){
        pinMode(DRpin, INPUT);
    }
    if(INTpin != NOPIN){
        pinMode(INTpin, INPUT);
    }

    pinMode(CSpin, OUTPUT);
    digitalWrite(CSpin, HIGH);

    if(TRXSTNDBYpin != NOPIN){
        pinMode(TRXSTNDBYpin, OUTPUT);
        digitalWrite(TRXSTNDBYpin, LOW);
    }
    if(TX0RTSpin != NOPIN){
        pinMode(TX0RTSpin, OUTPUT);
        digitalWrite(TX0RTSpin, HIGH);
    }
    if(TX1RTSpin != NOPIN){
        pinMode(TX1RTSpin, OUTPUT);
        digitalWrite(TX1RTSpin, HIGH);
    }
    if(TX2RTSpin != NOPIN){
        pinMode(TX2RTSpin, OUTPUT);
        digitalWrite(TX2RTSpin, HIGH);
    }
}

void mcp2515::init(CnfSet& cfg, bool oneShot){
    reset();
    delay(250);
    setMode(MODE_CONFIG);
    delay(250);

    //Disable RX buffer filters (accept all messages)
    bitModify(REG_RXB0CTRL, MASK_RXB0CTRL_RXM, 0x60);

    //Enable One-Shot transmission mode (disabled by default)
    if(oneShot){
        bitModify(REG_CANCTRL, MASK_CANCTRL_OSM, OSM_ENABLE);
    }
    delay(250);

    //Configure clock speed
    setupClock(cfg);

    delay(250);
    setMode(MODE_NORMAL);
}

int8_t mcp2515::sendMsgBuf(CANmsg *msg, uint8_t TXbuffNum){
    switch(TXbuffNum){
      case 0:
        loadT0(msg);
        break;
      case 1:
        loadT1(msg);
        break;
      case 2:
        loadT2(msg);
        break;
      default:
        return -1;
    }
    return RTS(TXbuffNum);
}

int8_t mcp2515::sendMsgBufData(CANmsg *msg, uint8_t TXbuffNum){
    switch(TXbuffNum){
      case 0:
        loadT0data(msg);
        break;
      case 1:
        loadT1data(msg);
        break;
      case 2:
        loadT2data(msg);
        break;
      default:
        return -1;
    }
    return RTS(TXbuffNum);
}

uint8_t mcp2515::dataAvailable(){
    uint8_t r = 0;
    if(INTpin != NOPIN){
        if(digitalRead(INTpin)){
            /* INTpin is active low, so a '1' here indicates a no-interrupt condition
             * If the interrupt pin is available, but not set, an RX condition
             * is not possible. */
            return 0;
        }else{
            //If the interrupt pin low, we then check if specific RX pins are
            //availabile.  If both are available, we can check them to see if a
            //RX interrupt was triggered specifically.
            if((RX0BFpin != NOPIN) && (RX1BFpin != NOPIN)){
                if(!digitalRead(RX0BFpin)){
                    r |= 0x01;
                }
                if(!digitalRead(RX1BFpin)){
                    r |= 0x02;
                }
                return r;
            }
            //If the status pins aren't available, fall through and use SPI
            //detection
        }
    }
    //Mask out all but the RXn Flag bits from the status byte
    return readStatus() & (RDSTAT_RX0IF | RDSTAT_RX1IF);
}

int8_t mcp2515::readMessage(uint8_t channel, CANmsg *msg){
    switch(channel){
        case 0:
            readR0data(msg);
            return 0;
        case 1:
            readR1data(msg);
            return 0;
    }
    return -1;
}

void printCanMsg(CANmsg *msg){
    Serial.print("ID: ");
    Serial.print(msg->getID(), HEX);
    Serial.print("  |  ");
    Serial.print("MSG: ");
    for(uint8_t i = 0; i < msg->getLength(); i++){
        Serial.print(msg->getData(i), HEX);
        Serial.print(" ");
    }
    Serial.print("\r\n");
}

CnfSet C16M_B1M   = {.cnf1 = 0x00, .cnf2 = 0xD0, .cnf3 = 0x82};
//C16M_B800K = {.cnf1 = , .cnf2 = , .cnf3 = };
CnfSet C16M_B500K = {.cnf1 = 0x00, .cnf2 = 0xF0, .cnf3 = 0x86};
CnfSet C16M_B250K = {.cnf1 = 0x41, .cnf2 = 0xF1, .cnf3 = 0x85};
CnfSet C16M_B125K = {.cnf1 = 0x03, .cnf2 = 0xF0, .cnf3 = 0x86};
CnfSet C16M_B100K = {.cnf1 = 0x03, .cnf2 = 0xFA, .cnf3 = 0x87};
CnfSet C16M_B50K =  {.cnf1 = 0x07, .cnf2 = 0xFA, .cnf3 = 0x87};
CnfSet C16M_B20K =  {.cnf1 = 0x0F, .cnf2 = 0xFF, .cnf3 = 0x87};
CnfSet C16M_B10K =  {.cnf1 = 0x1F, .cnf2 = 0xFF, .cnf3 = 0x87};

#ifdef STANDALONE
void showMsg(CANmsg *c){
    uint8_t *d = c->getDataBuf();
    uint8_t len = c->getLength();

    printf("SH.SL.E8.E0.DL\n");
    for(int i = 0; i < 5; i++){
        printf("%02X.", c->getCtrl(i));
    }
    printf("\n");

    printf("ID:  %i\n", c->getID());
    printf("RTR: %i\n", c->getRemoteTransmit());
    printf("MSG: ");
    for(int i = 0; i < len; i++){
        printf("%02X.", d[i]);
    }
    printf("\n");
}

int main(){
    loudWrite = 1;
    CANmsg c1 = CANmsg();
    showMsg(&c1);
    printf("\n");

    uint8_t data[8] = {0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88};
    CANmsg c2 = CANmsg(ID_STD_MAX, 0, 8, data);
    showMsg(&c2);
    printf("\n");

    CANmsg c3 = CANmsg(ID_EXT_MAX, 1, 8, data);
    showMsg(&c3);
    printf("\n");

    CANmsg c4 = CANmsg(28, 0);
    showMsg(&c4);
    printf("\n");

    mcp2515 mcp = mcp2515(10);
    mcp.readR0(&c1);
    mcp.readR1data(&c1);

    mcp.loadT0(&c2);

    mcp.RTS1();

    return 0;
}
#endif
